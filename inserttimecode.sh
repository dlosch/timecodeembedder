rm output.wav
rm temp.mp4
ffmpeg -i "$1" -ac 1 output.wav
timecode="$(./ltc-tools/ltcdump -f 30 output.wav | sed -n 3p | awk '{print $2}')"
ffmpeg -i "$1" -c copy -timecode $timecode temp.mp4
mv temp.mp4 "$1"

echo "$1: Timecode $timecode"
